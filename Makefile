SRC_DIR = src
_SRC_FILES = table.h array.h ini.h logger.h util.h
SRC_FILES = $(patsubst %,$(SRC_DIR)/%,$(_SRC_FILES))
OBJ_FILES = $(patsubst %.h,%.o,$(SRC_FILES))
II_DIR = /usr/include/libcutil
CFLAGS += -fPIC -shared

$(SRC_DIR)/%.o: %.c $(SRC_FILES)
	$(CC) -c -o $@ $< $(CFLAGS) 
build: $(OBJ_FILES)
	$(CC) -o libcutil.so $^ $(CFLAGS)
install:
	install -D -t /usr/include/libcutil src/*.h
	install libcutil.so /usr/lib/libcutil.so
clean:
	rm libcutil.so 2>/dev/null || true

all: build
.PHONY: clean install
