#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "array.h"

array_t* array_new() {
    array_t* c = malloc(sizeof(array_t));
    c->items = NULL;
    c->len = 0;
    return c;
}
void array_insert(array_t* arr, void* item) {
    arr->len++;
    arr->items = realloc(arr->items, arr->len * sizeof(void*));
    arr->items[arr->len-1] = item;
}
void array_pop(array_t* arr, int index) {
    if(arr->len < index)
        return;
    arr->len--;
    free(arr->items[index]);
    arr->items[index] = arr->items[arr->len];
}
unsigned int array_has_str(array_t* arr, char* data) {
    for(int i = 0; i < arr->len; i++) {
        if(strcmp(arr->items[i], data) == 0) {
            return 1;
        }
    }
    return 0;
}
// >.<
char* array_str_join(array_t* arr, const char* sep) {
    char* s = NULL;
    int i, c = 0, d = 0;
    if(arr->len < 1)
        return "";
    for(i = 0; i < arr->len; i++) {
        if(strlen(arr->items[i]) < 0)
            continue;
        c += strlen(arr->items[i])+strlen(sep);
    }
    if(!(s = malloc(c+1)))
        return NULL;
    memset(s, 0, c+1);
    for(i = 0; i < arr->len; i++) {
        strcat(s, arr->items[i]);
        if(i != arr->len-1)
            strcat(s, sep);
    }
    return s;
}
void array_free(array_t* arr, short pop) {
    int i = -1;
    if(pop) {
        for(i = 0; i < arr->len; i++) {
            free(arr->items[i]);
        }
    }
    free(arr->items);
    free(arr);
    arr = NULL;
}
