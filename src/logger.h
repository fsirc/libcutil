#ifndef H_LOGGER
#define H_LOGGER
typedef struct {
    char* filename;
    short print_stdout;
} logger_t;
enum logger_level {
    LOG_LEVEL_INFO = 0,
    LOG_LEVEL_ERR = 1,
    LOG_LEVEL_DEBUG = 2
};
logger_t* logger_new(char* filename, short print_stdout);
void logger_log(logger_t* logger,
                enum logger_level level,
                const char* root,
                const char* format, ...);
void logger_free(logger_t* logger);
#endif
