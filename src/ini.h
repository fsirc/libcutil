#include "table.h"
#ifndef H_INI
#define H_INI
typedef table_t ini_t;
struct ini_key {
    char name[100+1];
    char val[100+1];
};
struct ini_section {
    char name[100+1];
    // struct ini_key
    table_t* keys;
};
ini_t* ini_parse_fd(int fd);
char* ini_lookup_key(ini_t* secs, const char* section, 
        const char* name, unsigned int sindex, unsigned int index);
unsigned int ini_section_count(ini_t* secs, const char* section);
void ini_free(ini_t* secs);
#endif
