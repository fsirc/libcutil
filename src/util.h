#ifndef H_CUTIL_UTIL
#define H_CUTIL_UTIL
char* cutil_read_eof(int fd);
int cutil_read_line(int fd, char* buf, unsigned int max_size);
#endif
