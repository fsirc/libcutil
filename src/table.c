#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "array.h"
#include "table.h"
/* Table functions */
static unsigned int table_hash(const void *val) {
    // FNV-0
    const int h_prime = 16777619U, h_offset = 2166136261U;
    uint32_t hash = h_offset;
    for(const char* p = val; *p; p++) {
        hash *= h_prime;
        hash ^= (uint32_t)*p;
    }
    return hash;
}
table_t* table_new() {
    return array_new();
}
unsigned int table_key_exists(table_t* tbl, const void* key) {
    uint32_t h = table_hash(key);
    for(int i = 0; i < tbl->len; i++) {
        if(((struct array_hash_tbl_entry*)tbl->items[i])->hash == h) {
            return 1;
        }
    }
    return 0;
}
void* table_lookup_index(table_t* tbl, unsigned int index) {
    if(!tbl->items || tbl->len <= index)
        return NULL;
    return ((struct array_hash_tbl_entry*)tbl->items[index])->val;
}
void* table_lookup_key(table_t* tbl, const void* key) {
    const uint32_t lookup_hash = table_hash(key);
    for(int i = 0; i < tbl->len; i++) {
        if(((struct array_hash_tbl_entry*)tbl->items[i])->hash == lookup_hash)
            return ((struct array_hash_tbl_entry*)tbl->items[i])->val;
    }
    return NULL;
}
void table_rename_key(table_t* tbl, const void *key, const void* new_key) {
    const uint32_t new_hash = table_hash(new_key), old_hash = table_hash(key);
    for(int i = 0; i < tbl->len; i++)
        if(((struct array_hash_tbl_entry*)tbl->items[i])->hash == old_hash)
            ((struct array_hash_tbl_entry*)tbl->items[i])->hash = new_hash;
}
void table_insert(table_t* tbl, const void* key, void* val) {
    const uint32_t entry_hash = table_hash(key);
    struct array_hash_tbl_entry* e;
    if(!(e = malloc(sizeof(struct array_hash_tbl_entry)))) {
        return;
    }
    memset(e, 0, sizeof(struct array_hash_tbl_entry));
    e->hash = entry_hash, e->val = val;
    array_insert(tbl, e);
}
table_t* table_del(table_t* tbl, const void* key) {
    table_t* ret_tbl = tbl;
    uint32_t search_hash = table_hash(key);
    for(int i = 0; i < tbl->len; i++) {
        if(((struct array_hash_tbl_entry*)tbl->items[i])->hash == search_hash) {
            array_pop(tbl, i);
            break;
        }
    }
    return ret_tbl;
}
void table_free(table_t* tbl, short del) {
    int i = -1;
    if(del) {
        for(i = 0; i < tbl->len; i++) {
            free(table_lookup_index(tbl, i));
        }
    }
    array_free(tbl, 1);
}
