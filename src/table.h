#include <stdint.h>
#include "array.h"
#ifndef H_TBL
#define H_TBL
struct array_hash_tbl_entry {
    uint32_t hash;
    void *val;
};
typedef array_t table_t;
table_t* table_new();
void table_insert(table_t* tbl, const void* key, void* val);
unsigned int table_key_exists(table_t* tbl, const void* key);
void table_rename_key(table_t* tbl, const void *key, const void* new_key);
table_t* table_del(table_t* tbl, const void* key);
void* table_lookup_index(table_t* tbl, unsigned int index);
void* table_lookup_key(table_t* tbl, const void* key);
void table_free(table_t* tbl, short del);
#endif
