#include <stdint.h>
#ifndef H_ARRAY
#define H_ARRAY
typedef struct {
    void** items;
    uint32_t len;
} array_t;
array_t* array_new();
void array_free(array_t* arr, short pop);
void array_insert(array_t* arr, void* item);
void array_pop(array_t* arr, int index);
unsigned int array_has_str(array_t* arr, char* data);
char* array_str_join(array_t* arr, const char* sep);
#endif
