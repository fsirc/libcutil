#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "array.h"
#include "table.h"
#include "util.h"
#include "ini.h"
/* INI parser */

ini_t* ini_parse_fd(int fd) {
    char* sep = NULL, buf[256+1];
    ini_t* secs = NULL;
    array_t* skeys = NULL, *ssecs = NULL;
    struct ini_section* sec = NULL;
    struct ini_key* skey = NULL;
    int i = 0, lsep = 0;
    if(!(secs = table_new()))
        return NULL;
    while((i = cutil_read_line(fd, buf, 256)) > 0) {
        if(i >= 2 && i <= sizeof(((struct ini_section*)0)->name)
                && buf[0] == '[' && buf[i-1] == ']') {
            sec = malloc(sizeof(struct ini_section));
            memset(sec, 0, sizeof(struct ini_section));
            sec->keys = table_new();
            strncpy(sec->name, buf+1, i-2);
            if(!(ssecs = table_lookup_key(secs, sec->name))) {
                ssecs = array_new();
                table_insert(secs, sec->name, ssecs);
            }
            array_insert(ssecs, sec);
        } else if(sec &&
                  (sep = strchr(buf, '=')) &&
                  (lsep = strlen(sep)) >= 2 && i-lsep <=
                  sizeof(((struct ini_key*)0)->val)) {
            skey = malloc(sizeof(struct ini_key));
            memset(skey, 0, sizeof(struct ini_key));
            strncpy(skey->name, buf, i-lsep);
            strncpy(skey->val, sep+1, sizeof(skey->val));
            if(!(skeys = table_lookup_key(sec->keys, skey->name))) {
                skeys = array_new();
                table_insert(sec->keys, skey->name, skeys);
            }
            array_insert(skeys, skey);
        }
    }
    return secs;
}

unsigned int ini_section_count(ini_t* secs, const char* section) {
    unsigned int i = 0;
    array_t* ssecs = NULL;
    if((ssecs = table_lookup_key(secs, section)))
        i = ssecs->len;
    return i;
}

char* ini_lookup_key(ini_t* secs, const char* section,
                     const char* name, unsigned int sindex, unsigned int index) {
    struct ini_section* sec = NULL;
    array_t* keys = NULL, *ssecs = NULL;
    if(!(ssecs = table_lookup_key(secs, section)) || ssecs->len < sindex)
        return NULL;
    sec = ssecs->items[sindex];
    if((!(keys = table_lookup_key(sec->keys, name))) || keys->len < index)
        return NULL;
    return ((struct ini_key*)keys->items[index])->val;
}

void ini_free(ini_t* secs) {
    int i = -1, d = -1, f = -1;
    array_t* ssecs = NULL;
    struct ini_section* sec = NULL;
    if(!secs) return;
    for(i = 0; i < secs->len; i++) {
        ssecs = table_lookup_index(secs, i);
        for(f = 0; f < ssecs->len; f++) {
            sec = ssecs->items[f];
            for(d = 0; d < sec->keys->len; d++)
                array_free(table_lookup_index(sec->keys, d), 1);
            table_free(sec->keys, 0);
        }
        array_free(ssecs, 1);
    }
    table_free(secs, 0);
}
