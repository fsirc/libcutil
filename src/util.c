#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char* cutil_read_eof(int fd) {
    int offset = 0, r = -1, fsize = 1024+1;
    char* s = NULL, *d = NULL;
    if(!(s = malloc(fsize)))
        return NULL;
    memset(s, 0, fsize);
    while((r = read(fd, s+offset, 1024)) > 0) {
        fsize += 1024;
        if(!(d = realloc(s, fsize)))
            return NULL;
        memset(d+(fsize-1024), 0, 1024);
        s = d, offset += r;
    }
    s[fsize-1] = '\0';
    return s;
}

int cutil_read_line(int fd, char* buf, unsigned int max_size) {
    int offset = 0;
    memset(buf, 0, max_size);
    for(; read(fd, buf+offset, 1) >= 0 &&
            offset < max_size; offset++) {
        if(buf[offset] == '\n') {
            buf[offset] = '\0';
            return offset;
        }
    }
    return -1;
}
