#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "logger.h"

static char* logger_levels[] = {"info", "error", "debug"};

// If filename == NULL, print to stdout
logger_t* logger_new(char* filename, short print_stdout) {
    logger_t* logger = NULL;
    if(!(logger = malloc(sizeof(logger_t)))) {
        fprintf(stderr, "[logger] failed to initialise\n");
        return NULL;
    }
    logger->filename = filename;
    logger->print_stdout = print_stdout;
    return logger;
}

void logger_log(logger_t* logger,
                enum logger_level level, const char* root,
                const char* format, ...) {
    int fd = -1;
    time_t cur_time = time(NULL);
    struct tm* time_info = NULL;
    char fmt[1024] = {0}, fstr[1024] = {0}, dstr[200+1] = {0};
    va_list args;
    if(logger->filename != NULL) {
        fd = open(logger->filename, O_APPEND|O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP);
        if(fd == -1) {
            fprintf(stderr, "[logger] failed to open log file: %s\n", logger->filename);
            return;
        }
    }
    va_start(args, format);
    time_info = gmtime(&cur_time);
    if(strftime(dstr, sizeof(dstr), "%T %F", time_info) < 1) {
        fprintf(stderr, "[logger] failed to format the date\n");
        return;
    }
    snprintf(fmt, sizeof(fmt), "%s [%s][%s] %s\n",
             dstr, logger_levels[level], root, format);
    vsnprintf(fstr, sizeof(fstr), fmt, args);
    if(!logger->filename || logger->print_stdout)
        printf("%s", fstr);
    if(logger->filename)
        write(fd, fstr, strlen(fstr));
    va_end(args);
    if(fd != -1)
        close(fd);
}

void logger_free(logger_t* logger) {
    assert(logger);
    free(logger);
}
